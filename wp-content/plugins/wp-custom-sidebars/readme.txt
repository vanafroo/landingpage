=== WP Custom Sidebars ===
Contributors: mnmlthms
Donate link: http://mnmlthms.com/
Tags: custom-sidebars, widget, widgets, sidebar, sidebars, personalize
Requires at least: 4.0
Tested up to: 4.6.1
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Create unlimited sidebars for pages/posts easily without writing a single line of code!

== Description ==

If you are looking for a plugin that help you to create unlimited sidebars for pages/post without coding skills, WP Custom Sidebars is what you're looking for!.

Easily overriding sidebar positions on your site, no integration needed. Works with any WordPress theme.

**Features:**

* Configurable supported post types.
* Adding as many sidebars as you want.
* Transferable/Importable sidebar data.
* Translation ready with mo/po files.

*Note that if you are using other custom sidebar plugins, you should disable them so WP Custom Sidebars won't be confused on overriding sidebars :D*

**How to use WP Custom Sidebars?**

* Step 1: Select post types you want to support custom sidebars in Appearance -> Sidebars -> General Settings tab.
* Step 2: Create as many sidebars as you want. Then go to Appearance -> Widgets and add widgets to your sidebar you've just created.
* Step 3: Go edit page/post, then look for WP Custom Sidebars setting box, and overriding sidebars.

That's it!

== Frequently Asked Questions ==

= Does this plugin support custom post type? =

Yes, it does. Please go to Appearance -> Sidebars -> Settings and choose post types you wish to support.

= Does this plugin support archive pages such as categories or tags? =

Currently it doesn't but we will work on it in future.

== Screenshots ==

1. Adding custom sidebars
2. Plugin Settings
3. Overriding sidebars

== Changelog ==

= 1.0 =
* Initial Release

== Upgrade Notice ==

= 1.0 =

== Arbitrary section ==